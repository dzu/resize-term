# resize-term

Small Python tool without any dependency to adapt a shell session on a
serial terminal to the correct window size.

This tool is a replacement for the `resize` tool from the xterm code
base and fixes the common problem of garbled output on the command
line when running a serial shell session in a non 80x24 terminal.

It works by using [Escape
sequences](https://en.wikipedia.org/wiki/ANSI_escape_code) interpreted
by the terminal to read out the number of columns and lines.  First
the cursor position is saved, then the cursor is moved to line 999 and
column 999.  This movement will be truncated by the terminal to the
maximum allowed amount.  The terminal is then instructed to output the
current cursor position and the result is parsed and set as the
current window size with
[TIOCSWINSZ](https://man7.org/linux/man-pages/man4/tty_ioctl.4.html).
Finally the cursor is restored to its original position.

The running shell will pick up the changed window size before the next
prompt and adjust the environment variables `LINES` and `COLUMNS`
accordingly.  This way it is not neccesary to do the old 

```
$ eval `resize`
```

dance but simply execute the tool.

Terminal Emulators known to work correctly:

- Gnome Terminal
- XTerm
